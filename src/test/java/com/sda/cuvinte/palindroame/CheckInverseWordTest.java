package com.sda.cuvinte.palindroame;

import org.junit.Assert;
import org.junit.Test;

public class CheckInverseWordTest {

    @Test
    public void checkInverseTestTrue() {

        CheckInverseWord myChecker = new CheckInverseWord("ana are mere era erem ana pot top oou uoo");

        Assert.assertTrue(myChecker.checkInverse());
    }

    @Test
    public void checkInverseTestFalse() {

        CheckInverseWord myChecker = new CheckInverseWord("ana are mere era erem ana pot top oou ulppoo");

        Assert.assertFalse(myChecker.checkInverse());
    }
}
