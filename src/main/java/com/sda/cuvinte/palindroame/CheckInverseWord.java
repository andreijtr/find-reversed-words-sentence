package com.sda.cuvinte.palindroame;

public class CheckInverseWord {

    //sentence to check
    private String sentence;

    public CheckInverseWord(String sentence) {
        this.sentence = sentence;
    }

    //this method checks if every word from sentence has a reversed
    public boolean checkInverse() {

        //at start suppose every word has his reversed
        boolean hasInverse = true;

        //count how many times the reversed is found in array
        int count = 0;

        //for every word will compute the reverse and hold in this variable
        String wordReversed;

        //split sentence in words
        String[] splitSentence = this.sentence.split(" ");

        //for array parsing
        int i = 0;
        int j = 0;

        //while index i is not at end of array && every word checked until now has a reversed
        while (i < splitSentence.length && hasInverse) {
            wordReversed = Inverse.createStringInverse(splitSentence[i]);
            count = 0;

            //suppose this word doesn't have a reversed
            hasInverse = false;

            j = 0;
            //parse the array from index 0 to end and while no reversed was found, search for this reversed
            while (j < splitSentence.length && count < 1) {

                //if reversed is found, increase count so above "while"  will finish, is no reason to parse till the end of array
                //hasInverse becomes true, so we have to next i index
                //if no reversed is found, then outter "while" will stop, because is no reason to go further, one word has no reversed
                if ( j != i && splitSentence[j].equals(wordReversed)) {
                    count++;
                    hasInverse = true;
                }
                j++;
            }
            i++;
        }

        //if any word has a reversed pair, hasInverse will be true at end of function
        //but if one word has no reversed, "while" block will be finished and return value will be false
        return hasInverse;
    }
}
