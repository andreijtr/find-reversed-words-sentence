package com.sda.cuvinte.palindroame;

public class Inverse {

    // this method creates and returns a reverse for a given string
    public static String createStringInverse(String word) {

        String reversed = "";
        int i = (word.length() - 1);

        while ( i >= 0) {
            reversed = reversed.concat(String.valueOf(word.charAt(i)));
            i--;
        }
        return reversed;
    }
}